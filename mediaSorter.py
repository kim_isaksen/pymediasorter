#!/usr/bin/env python
__doc__ = """Organise your photos.

Lysalf, 2014.03.27, 

Idea inspired by https://code.google.com/p/py-photo-organiser/

NAME
    mediaSorter - sorting media files according to their take/created/cam model date using EXIF info

SYNOPSIS
    [python] mediaSorter [OPTION]... [SOURCE] [DESTINATION]

EXAMPLE
    [python] mediaSorter [-r] [-v]

DESCRIPTION
    copy or move a directory tree of photos recursively and put the photos into
    direcctories of their own date. A photo file that was taken at YYYY/MM/DD
    will end up in a folder of YYYY/CAM_MODEL/MM (posix) YYYY/CAM_MODEL/MM (win)

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -v, --verbose         make lots of noise [default]
  -q, --quiet           unless errors don't output anything
  -r, --recursive       operate recursively [default]
  -s, --symlink         follow symbolic linked directories
  -m, --move            delete original file from SOURCE
  -d DEPTH, --depth=DEPTH
                        unlimited [default: 0]
  -g LOG, --log=LOG     log all actions [default: sys.stderr]
  -i, --ignore          ignore photos with missing EXIF header [default]
  -P NOEXIFPATH, --process-no-exif=NOEXIFPATH
                        copy/moves images with no EXIF data to [default:NO_EXIF]
  -V VIDEOPATH, --process-no-video-exif=VIDEOPATH
                        copy/moves videos with no EXIF data to [default:NO_VIDEO_EXIF]

"""
from optparse import OptionParser
from datetime import datetime, timedelta
#from PIL import Image
from gi.repository import GExiv2
#import pyexiv2 //could be used if GExiv2 cannot be installed!
import exiftool #perl wrapper, to parse video files
import sys
import os
import shutil
import string
import uuid
import hashlib
import logging, logging.handlers

__version__ = "0.104"

class ImageException(Exception):
    pass

class InvalidDateTag(Exception):
    pass

class MissingMediaFile(Exception):
    pass

class MissingVideoFile(Exception):
    pass
   
def isPhoto(path):
    knownPhotoExt = [".jpg",".jpeg",".png",".nef",".gif",".tiff"]
    _, ext = os.path.splitext(path)
    if string.lower(ext) in knownPhotoExt:
        return True
    else:
        return False

def isVideo(path):
    knownPhotoExt = [".ogg",".mpg",".mp4",".3gp",".avi",".mov"]
    _, ext = os.path.splitext(path)
    if string.lower(ext) in knownPhotoExt:
        return True
    else:
        return False

def isExact(file1, file2):
    ''' checks if two files have exactly same content, i cheat and checks only 1st 1kb of each file for performance reasons   
    '''
    # full check
    #hash1 = hashlib.sha256()
    #hash2 = hashlib.sha256()
    #hash1.update(open(file1).read())
    #hash2.update(open(file2).read())
    #if hash1.hexdigest() == hash2.hexdigest():

    f1 = Checksum(file1)
    f2 = Checksum(file2)
    if f1 == f2:
        # files are exact
        logging.info("%s and %s are exact duplicate" %(file1, file2))
        return True
    else:
        return False

def Checksum(current_file_name, check_type = 'sha512', first_block = False):
    """Computes the hash for the given file. If first_block is True,
    only the first block of size size_block is hashed."""
    size_block = 1024 * 1024 # The first N bytes (1KB)

    d = {'sha1' : hashlib.sha1, 'md5': hashlib.md5, 'sha512': hashlib.sha512}

    if(not d.has_key(check_type)):
        raise Exception("Unknown checksum method")

    file_size = os.stat(current_file_name).st_size
    with file(current_file_name, 'rb') as f:
        key = d[check_type].__call__()
        while True:
            s = f.read(size_block)
            key.update(s)
            file_size -= size_block
            if(len(s) < size_block or first_block):
                break
            return key.hexdigest().upper()

def getRandomString(string_length=10):
    """Returns a random string of length string_length."""
    random = str(uuid.uuid4()) # Convert uuid format to python string.        
    random = random.lower() # Make all characters uppercase.
    random = random.replace("-","") # Remove the uuid '-'.        
    return random[0:string_length] # Return the random string.

class MediaFile:
    ''' a file that contains valid media format
    '''
    def __init__(self, fullfilepath, mediaType):
        ''' creates an instance of the MediaFile
        '''
        self.mediaType = mediaType
        if not os.path.exists(fullfilepath): # file missing
            raise MissingMediaFile('file not found %s' %fullfilepath)
        else:
            self.srcfilepath = fullfilepath

        self.getExifDate()#get the datetime stamp

        try:
            self.dateStamp = MediaDate(self.mediaDate)
        except AttributeError:
            logger.info("IGNORE THIS FILE %s",fullfilepath)

    def getExifDate(self):
        if self.mediaType == 'photo': #use GExiv2 for speed when testing for photos
            try:
                exifdata = GExiv2.Metadata(fullfilepath)
                taken_date = exifdata.get('Exif.Photo.DateTimeOriginal')
                makestring = exifdata.get('Exif.Image.Model')
            except:
                return

            if type(taken_date) == type(None):
                taken_date = exifdata.get('Exif.Image.DateTime')
            if type(taken_date) == type(None):
                logger.info("date is unknown, move image to /%s/ folder",options.noExifPath)
        elif self.mediaType == 'video':
            exif = exiftool.ExifTool()
            exif.start()    
            taken_date = exif.get_tag('QuickTime:CreateDate',fullfilepath);
            makestring = exif.get_tag('File:FileType',fullfilepath);

            if type(taken_date) == type(None):
                logger.info("date is unknown, move video to /%s/ folder",options.videoPath)

        if taken_date == None or taken_date == '0000:00:00 00:00:00' or taken_date.strip() == '':
            self.mediaDate = None;
        else:
            self.mediaDate = self.stringEncode(taken_date)
            
        if makestring == None or makestring == '':
            self.modelString = None;
        else:
            self.modelString = self.stringEncode(makestring)
   
   
    def stringEncode(self, string):
        try:
            return string.encode('ascii','ignore')
        except:
            return string
   
    def getFileName(self, base):
        ''' gets a proper name and path with no duplications
        '''
	try:
           test = datetime.strptime(self.mediaDate, '%Y:%m:%d %H:%M:%S')
	except:
           self.mediaDate = None

        if self.mediaDate is None or self.modelString is None:
            # i should handle images with no dateStamp here, using the error folders defined in -V & -P
            _, fileName = os.path.split(self.srcfilepath.lower())
            self.dstfilename = self.placeUnknown(base, fileName)
            if os.path.exists(self.dstfilename):
                logger.info("media already exists %s " %self.dstfilename)                
                if isExact(self.srcfilepath, self.dstfilename):
                    logger.info("and its duplicate %s " %self.dstfilename)
                    return None # duplicate
                else:
                    logger.info("here we need to rename the file cause file is not a dublet %s " %self.dstfilename)
                    fileName = getRandomString() + '_' + fileName
                    return self.placeUnknown(base, fileName)                    
            else:
                return self.dstfilename
        
        while True:
            self.dstfilename = self.dateStamp.getPath(base, self.srcfilepath, self.modelString)
            if os.path.exists(self.dstfilename):
                logger.info("media already exists %s " %self.dstfilename)                
                if isExact(self.srcfilepath, self.dstfilename):
                    logger.info("and its duplicate %s " %self.dstfilename)
                    return None # duplicate
                else:
                    logger.info("but not duplicate %s " %self.dstfilename)
                    self.dateStamp.incMicrosecond()
            else:
                return self.dstfilename

    def placeUnknown(self, base, fileName):
        filePath = os.path.split(self.srcfilepath)        
        arr = filePath[0].split('/')
        arr.pop(0)
        fileLocation = os.path.join(str("/".join(arr)),fileName)
        if self.mediaType == 'photo':
            dstfilename = os.path.join(base, options.noExifPath,fileLocation)
        else:
            dstfilename = os.path.join(base, options.videoPath,fileLocation)

        return dstfilename

    def move(self, base, deleteOriginal=None):
        if self.getFileName(base) is None:
            logger.info("unknown destination for %s, File is ignored  " %self.srcfilepath)
            return
        
        dstdir = os.path.dirname(self.dstfilename)        
        if not (os.path.exists(dstdir) and os.path.isdir(dstdir)):
            logger.info("creating dir %s" %dstdir)
            os.makedirs(dstdir)
        if deleteOriginal:
            logger.info("moving %s ==> %s " %(self.srcfilepath, self.dstfilename))
            shutil.move(self.srcfilepath, self.dstfilename)
        else:
            logger.info("copying %s ==> %s " %(self.srcfilepath, self.dstfilename))
            shutil.copy(self.srcfilepath, self.dstfilename)

class MediaDate:
    '''
        a date and time class
    '''
    def __init__(self, media_dt_tm=None):    
        '''
            creates an instance using the given file media_dt_tm
        '''
        try:            
            if type(media_dt_tm) == type(''): # string
                self.media_dt_tm = datetime.strptime(media_dt_tm, '%Y:%m:%d %H:%M:%S')
                # self.img_dt_tm.microsecond = 0
            else:                
                self.media_dt_tm = media_dt_tm
        except ValueError, e:
            logging.info('Missing date/time')

    def getPath(self, base, filename, modelstring):        
        ''' returns a string that describes a path as a date such as
        year/month/day/hour_minute_second_microsecond.
        '''
        _, fileExt = os.path.splitext(filename.lower())
        fileName = self.media_dt_tm.strftime('%Y_%m_%d-%H_%M_%S_%f') + fileExt

        return os.path.join(base, \
                            str(self.media_dt_tm.year),
                            str(modelstring.strip()), 
                            str(self.media_dt_tm.month),fileName)

    def __str__(self):
        return str(self.media_dt_tm)

    def incMicrosecond(self):
        self.media_dt_tm += timedelta(microseconds=1)

def getOptions():
    ''' creates the options and return a parser object
    '''
    parser = OptionParser(usage="%prog [options] src dest", version="%prog 0.1")
    parser.add_option("-v", "--verbose",
                      action="store_true", dest="verbose",
                      default=True,
                      help="make lots of noise [default]")
    parser.add_option("-q", "--quiet",
                      action="store_false", dest="verbose",
                      help="unless errors don't output anything")
    parser.add_option("-r", "--recursive",
                      action="store_true", dest="recursive",
                      help="operate recursively [default]")
    parser.add_option("-s", "--symlink",
                      action="store_true", dest="symlink",
                      help="follow symbolic linked directories")
    parser.add_option("-m", "--move",
                      action="store_true", dest="move",
                      help="delete original file from SOURCE, by default it makes a copy of the file")
    parser.add_option("-d", "--depth",
                      default="0", dest="depth",
                      help="unlimited [default: %default]")
    parser.add_option("-g", "--log",
                      default="sys.stderr", dest="log",
                      help="log all actions [default: %default]")
    parser.add_option("-i", "--ignore",
                      action="store_true", dest="ignore", default=True,
                      help="ignore photos with missing EXIF header [default]")
    parser.add_option("-P", "--process-no-exif",
                      default="NO_EXIF", dest="noExifPath",
                      help="copy/moves images with no EXIF data to [default: %default]")
    parser.add_option("-V", "--process-no-video-exif",
                      default="NO_VIDEO_EXIF", dest="videoPath",
                      help="copy/moves videos with no EXIF data to [default: %default]")

    return parser

class FilesTree:
    def __init__(self):
        pass
   
    def __next__(self):
        pass
   
    def __iter__(self):
        pass
   
def treewalk(top, followlinks=False, depth=0):
    ''' generator similar to os.walk(), but with limited subdirectory depth
    '''
    if not maxdepth is None:
        if  depth > maxdepth:
            return
    for file in os.listdir(top):
        file_path = os.path.join(top, file)
        if os.path.isdir(file_path):
            # its a dir recurse into it
            for dirpath, filename in treewalk(file_path, followlinks, depth+1):
                yield dirpath, filename
        elif os.path.isfile(file_path):
            yield top, file
        else:
            # Unknown file type, print a message
            logging.info('Skipping %s' % pathname)


if __name__=='__main__':
    ''' main
    '''
    
    
    
    parser = getOptions()
    (options, args) = parser.parse_args()

    logger = logging.getLogger('')
    if options.log == "sys.stderr":
        console = logging.StreamHandler()
        logger.addHandler(console)
    else:
        fileHandler = logging.FileHandler(options.log)
        logger.addHandler(fileHandler)
    if len(args) == 1:
        src = dst = args[0]
        # this needs to build the tree under temp and move it to dest when done
    elif len(args) == 2:
        src = args[0]
        dst = args[1]
    else:
        logging.error("invalid number of arguments")
        parser.print_help()
        sys.exit()

    if options.verbose:
        logger.setLevel(logging.DEBUG)
        logger.debug('verbose mode on')
    else:
        logger.debug('verbose mode off')

    if options.symlink:
        logger.debug("following symlinks")
    else:
        logger.debug("ignoring symlinks")
    maxdepth = options.depth
    logger.debug("depth is: "+ options.depth)
    if maxdepth == 0: 
        maxdepth = None
        
    for dirpath, filename in treewalk(top=src, followlinks=options.symlink):
        fullfilepath = os.path.join(dirpath, filename)
        logger.debug("--------------------------------")
        logger.debug("processing %s" %fullfilepath)
        
        #examine media item
        if isPhoto(fullfilepath):#foto test
            logger.debug("photo file detected, %s" %fullfilepath)
            mediafile = MediaFile(fullfilepath, 'photo')
        elif isVideo(fullfilepath):#video test
            logger.debug("video file detected, %s" %fullfilepath)
            mediafile = MediaFile(fullfilepath, 'video')
        else:#ignore this file            
            logger.debug("invalid file detected, %s" %fullfilepath)
            continue

        #lets start the circus, copy/move routine
        if hasattr(mediafile, 'mediaDate'):
            mediafile.move(dst, options.move)
        else:
            logger.info("no actions taken with %s ",fullfilepath)
        
        
    
