# One mediaSort script to rule them all.

Well imagine you like me have some 100 GB of photos and videos spread out on 3-4 phones, some external hard drives, a few old laptops, and a few newer ones.
A lot of backup of backups of backups of backups....

The goal is to make a dump folder. Dump all files there, from everywhere.

Start the "./mediaSorter dumpfolder/ my_sorted_data/" script and now all the files will appear in folders named 

* /2011/nikon D80/2/year_month_date_hour_min_sec.ext  # images from my nikon cam from feb 2011
* /2011/HTC Desire HD/11/year_month_date_hour_min_sec.ext # images from my phone from nov 2011
* /2008/AVI/1/year_month_date_hour_min_sec.ext # video files from jan 2008 in AVI format
* /NO_EXIF/orig_folders/filename.ext #catch folder for images
* /NO_VIDEO_EXIF/orig_folders/filename.ext #catch folder for videos

Script will check for doublettes, with a filename match and a 1KB hash from file start
